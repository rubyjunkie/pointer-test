# pointer-test

This is a RubyMotion 7.5 sample project that causes an error when using a Pointer.

You can see it in use in `file_sytem.rb`, `FileSystem.create_security_bookmark`

Run `rake spec` to run the test the causes the error.  Logging information
will be output to the console (which happens when a normal error is detected).
So this is expected:

```
FileSystem.create_security_bookmark
  The file “test_dir” couldn’t be opened because there is no such file.
  (code: 260, domain: NSCocoaErrorDomain, userInfo: {"NSURL"=>#<NSURL:0x10063b290>, "NSDebugDescription"=>"Scoped bookmarks can only be created for existing files or directories"})
```

However a few seconds after the test completes (and successfully), the following
macOS crash dialog is shown


```
Process:               pointer-test [16571]
Path:                  /Users/USER/*/pointer-test_spec.app/Contents/MacOS/pointer-test
Identifier:            com.yourcompany.pointer-test_spec
Version:               1.0 (1.0)
Code Type:             X86-64 (Native)
Parent Process:        repl [16570]
Responsible:           iTerm2 [2179]
User ID:               501

Date/Time:             2020-06-06 09:44:56.086 -0500
OS Version:            Mac OS X 10.15.5 (19F96)
Report Version:        12
Bridge OS Version:     3.0 (14Y908)
Anonymous UUID:        BBA0D32F-2C3E-C93C-45A6-A5D06E5E1CAE


Time Awake Since Boot: 430000 seconds

System Integrity Protection: enabled

Crashed Thread:        0  Dispatch queue: com.apple.main-thread

Exception Type:        EXC_BAD_INSTRUCTION (SIGILL)
Exception Codes:       0x0000000000000001, 0x0000000000000000
Exception Note:        EXC_CORPSE_NOTIFY

Termination Signal:    Illegal instruction: 4
Termination Reason:    Namespace SIGNAL, Code 0x4
Terminating Process:   exc handler [16571]

Application Specific Information:
Detected over-release of a CFTypeRef

Thread 0 Crashed:: Dispatch queue: com.apple.main-thread
0   com.apple.CoreFoundation      	0x00007fff37d44a7f _CFRelease.cold.3 + 14
1   com.apple.CoreFoundation      	0x00007fff37ce3a0d _CFRelease + 1234
2   libobjc.A.dylib               	0x00007fff70a7c054 AutoreleasePoolPage::releaseUntil(objc_object**) + 134
3   libobjc.A.dylib               	0x00007fff70a60dba objc_autoreleasePoolPop + 175
4   com.apple.CoreFoundation      	0x00007fff37bd5b15 _CFAutoreleasePoolPop + 22
5   com.apple.CoreFoundation      	0x00007fff37c32889 __CFRUNLOOP_IS_CALLING_OUT_TO_A_TIMER_CALLBACK_FUNCTION__ + 20
6   com.apple.CoreFoundation      	0x00007fff37c323ef __CFRunLoopDoTimer + 859
7   com.apple.CoreFoundation      	0x00007fff37c31ed7 __CFRunLoopDoTimers + 322
8   com.apple.CoreFoundation      	0x00007fff37c16c7a __CFRunLoopRun + 1871
9   com.apple.CoreFoundation      	0x00007fff37c15ece CFRunLoopRunSpecific + 462
10  com.apple.HIToolbox           	0x00007fff36844abd RunCurrentEventLoopInMode + 292
11  com.apple.HIToolbox           	0x00007fff368447d5 ReceiveNextEventCommon + 584
12  com.apple.HIToolbox           	0x00007fff36844579 _BlockUntilNextEventMatchingListInModeWithFilter + 64
13  com.apple.AppKit              	0x00007fff34e8c829 _DPSNextEvent + 883
14  com.apple.AppKit              	0x00007fff34e8b070 -[NSApplication(NSEvent) _nextEventMatchingEventMask:untilDate:inMode:dequeue:] + 1352
15  com.apple.AppKit              	0x00007fff34e7cd7e -[NSApplication run] + 658
16  com.apple.AppKit              	0x00007fff34e4eb86 NSApplicationMain + 777
17  com.yourcompany.pointer-test_spec	0x0000000100001ce9 main + 313
18  libdyld.dylib                 	0x00007fff71c12cc9 start + 1

Thread 1:
0   libsystem_kernel.dylib        	0x00007fff71d5a3be __accept + 10
1   com.yourcompany.pointer-test_spec	0x00000001001a2c96 -[PeerTalkWrapper init] + 294
2   com.yourcompany.pointer-test_spec	0x00000001001a3109 rb_repl_accept() + 41
3   com.yourcompany.pointer-test_spec	0x00000001001a16e3 LLIChildTarget::initialize() + 19
4   com.yourcompany.pointer-test_spec	0x00000001001a2884 repl_loop(void*) + 84
5   libsystem_pthread.dylib       	0x00007fff71e17109 _pthread_start + 148
6   libsystem_pthread.dylib       	0x00007fff71e12b8b thread_start + 15

Thread 2:
0   libsystem_pthread.dylib       	0x00007fff71e12b68 start_wqthread + 0

Thread 3:
0   libsystem_pthread.dylib       	0x00007fff71e12b68 start_wqthread + 0

Thread 4:
0   libsystem_pthread.dylib       	0x00007fff71e12b68 start_wqthread + 0

Thread 5:
0   libsystem_pthread.dylib       	0x00007fff71e12b68 start_wqthread + 0

Thread 6:: Dispatch queue: NSCGSDisableUpdates
0   libsystem_kernel.dylib        	0x00007fff71d53e4e semaphore_timedwait_trap + 10
1   com.apple.SkyLight            	0x00007fff66e54342 CGSUpdateManager::enable_updates_common() + 302
2   com.apple.SkyLight            	0x00007fff66e54a84 SLSReenableUpdateTokenWithSeed + 121
3   libdispatch.dylib             	0x00007fff71bb86c4 _dispatch_call_block_and_release + 12
4   libdispatch.dylib             	0x00007fff71bb9658 _dispatch_client_callout + 8
5   libdispatch.dylib             	0x00007fff71bbec44 _dispatch_lane_serial_drain + 597
6   libdispatch.dylib             	0x00007fff71bbf5d6 _dispatch_lane_invoke + 363
7   libdispatch.dylib             	0x00007fff71bc8c09 _dispatch_workloop_worker_thread + 596
8   libsystem_pthread.dylib       	0x00007fff71e13a3d _pthread_wqthread + 290
9   libsystem_pthread.dylib       	0x00007fff71e12b77 start_wqthread + 15

Thread 7:: com.apple.NSEventThread
0   libsystem_kernel.dylib        	0x00007fff71d53dfa mach_msg_trap + 10
1   libsystem_kernel.dylib        	0x00007fff71d54170 mach_msg + 60
2   com.apple.CoreFoundation      	0x00007fff37c17f85 __CFRunLoopServiceMachPort + 247
3   com.apple.CoreFoundation      	0x00007fff37c16a52 __CFRunLoopRun + 1319
4   com.apple.CoreFoundation      	0x00007fff37c15ece CFRunLoopRunSpecific + 462
5   com.apple.AppKit              	0x00007fff3502e144 _NSEventThread + 132
6   libsystem_pthread.dylib       	0x00007fff71e17109 _pthread_start + 148
7   libsystem_pthread.dylib       	0x00007fff71e12b8b thread_start + 15

Thread 0 crashed with X86 Thread State (64-bit):
  rax: 0x00007fff37f2ebf4  rbx: 0x0000000000601b80  rcx: 0x0000000100816f48  rdx: 0x00000000000b2450
  rdi: 0x000000010065be10  rsi: 0x00007fff7beab940  rbp: 0x00007ffeefbfc210  rsp: 0x00007ffeefbfc1b8
   r8: 0x0000000000002c50   r9: 0x0000000000000007  r10: 0x00007fff8f5948c0  r11: 0x00007fff37c10f07
  r12: 0x00007fff3a2e9543  r13: 0x0000000100515cd8  r14: 0x0000000100816e78  r15: 0xa3a3a3a3a3a3a3a3
  rip: 0x00007fff37d44a7f  rfl: 0x0000000000010207  cr2: 0x000000010002aff0

Logical CPU:     6
Error Code:      0x00000000
Trap Number:     6


Binary Images:
       0x100000000 -        0x1001beff7 +com.yourcompany.pointer-test_spec (1.0 - 1.0) <22DC657B-4BEE-30C5-A462-C5F1E4E0BB65> /Users/USER/*/pointer-test_spec.app/Contents/MacOS/pointer-test
       0x1012e5000 -        0x1012e8047  libobjc-trampolines.dylib (787.1) <8212B354-A106-3806-A022-A60F2B2FA5C9> /usr/lib/libobjc-trampolines.dylib
       0x106e82000 -        0x106f13eff  dyld (750.5) <26346F4C-B18E-31A1-9964-30736214F1BF> /usr/lib/dyld
    0x7fff3381f000 -     0x7fff3381ffff  com.apple.Accelerate (1.11 - Accelerate 1.11) <56DFF715-6A4E-3231-BDCC-A348BCB05047> /System/Library/Frameworks/Accelerate.framework/Versions/A/Accelerate
    0x7fff33820000 -     0x7fff33836fef  libCGInterfaces.dylib (524.2.1) <A423F932-C044-3CEB-BF20-BF4CB323361E> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vImage.framework/Versions/A/Libraries/libCGInterfaces.dylib
    0x7fff33837000 -     0x7fff33e8dfff  com.apple.vImage (8.1 - 524.2.1) <17C93AB9-1625-3FDB-9851-C5E77BBE3428> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vImage.framework/Versions/A/vImage
    0x7fff33e8e000 -     0x7fff340f5ff7  libBLAS.dylib (1303.60.1) <CBC28BE4-3C78-3AED-9565-0D625251D121> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
    0x7fff340f6000 -     0x7fff345c9fef  libBNNS.dylib (144.100.2) <8D653678-1F9B-3670-AAE2-46DFB8D37643> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBNNS.dylib
    0x7fff345ca000 -     0x7fff34965fff  libLAPACK.dylib (1303.60.1) <F8E9D081-7C60-32EC-A47D-2D30CAD73C5F> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLAPACK.dylib
    0x7fff34966000 -     0x7fff3497bfec  libLinearAlgebra.dylib (1303.60.1) <D2C1ACEA-2B6A-339A-9EEB-62A76CC92CBE> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLinearAlgebra.dylib
    0x7fff3497c000 -     0x7fff34981ff3  libQuadrature.dylib (7) <3112C977-8306-3190-8313-01A952B7F3CF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libQuadrature.dylib
    0x7fff34982000 -     0x7fff349f2fff  libSparse.dylib (103) <40510BF9-99A7-3155-A81D-6DE5A0C73EDC> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparse.dylib
    0x7fff349f3000 -     0x7fff34a05fef  libSparseBLAS.dylib (1303.60.1) <3C1066AB-20D5-38D2-B1F2-70A03DE76D0B> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparseBLAS.dylib
    0x7fff34a06000 -     0x7fff34bddfd7  libvDSP.dylib (735.121.1) <74702E2E-ED05-3765-B18C-64BEFF62B517> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvDSP.dylib
    0x7fff34bde000 -     0x7fff34ca0fef  libvMisc.dylib (735.121.1) <137558BF-503D-3A6E-96DC-A181E3FB31FF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvMisc.dylib
    0x7fff34ca1000 -     0x7fff34ca1fff  com.apple.Accelerate.vecLib (3.11 - vecLib 3.11) <D7E8E400-35C8-3174-9956-8D1B483620DA> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/vecLib
    0x7fff34ca2000 -     0x7fff34d01ff0  com.apple.Accounts (113 - 113) <0510C893-F1F2-38B2-B3AA-A30DB2F5B688> /System/Library/Frameworks/Accounts.framework/Versions/A/Accounts
    0x7fff34e4b000 -     0x7fff35c0bffd  com.apple.AppKit (6.9 - 1894.50.103) <61269B8C-C432-335F-8894-B95C235A41A5> /System/Library/Frameworks/AppKit.framework/Versions/C/AppKit
    0x7fff35c5b000 -     0x7fff35c5bfff  com.apple.ApplicationServices (48 - 50) <EEC73694-1A37-3C14-A839-6991E2BD8655> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices
    0x7fff35c5c000 -     0x7fff35cc7fff  com.apple.ApplicationServices.ATS (377 - 493.0.4.1) <A6912C4A-55CC-3701-BACA-E63423B99481> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/ATS
    0x7fff35d60000 -     0x7fff35d9eff0  libFontRegistry.dylib (274.0.5.1) <A78A7869-C96C-3AD6-A038-DE541639BB60> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/Resources/libFontRegistry.dylib
    0x7fff35df9000 -     0x7fff35e28fff  com.apple.ATSUI (1.0 - 1) <4B3C2201-DBB3-352C-936B-9C423122EFF6> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATSUI.framework/Versions/A/ATSUI
    0x7fff35e29000 -     0x7fff35e2dffb  com.apple.ColorSyncLegacy (4.13.0 - 1) <47D42CDE-2E9A-3AF6-9365-1BFD1189196B> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ColorSyncLegacy.framework/Versions/A/ColorSyncLegacy
    0x7fff35ec7000 -     0x7fff35f1effa  com.apple.HIServices (1.22 - 675.1) <273492E3-FF0F-3A8A-A83F-0F11F99B5F26> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/HIServices.framework/Versions/A/HIServices
    0x7fff35f1f000 -     0x7fff35f2dfff  com.apple.LangAnalysis (1.7.0 - 1.7.0) <DA175323-5BE3-3C54-92D4-A171A4A067E6> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/LangAnalysis.framework/Versions/A/LangAnalysis
    0x7fff35f2e000 -     0x7fff35f73ffa  com.apple.print.framework.PrintCore (15.4 - 516.2) <99AEBCDB-2DCA-3A13-906F-7F0D7962B002> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/PrintCore.framework/Versions/A/PrintCore
    0x7fff35f74000 -     0x7fff35f7eff7  com.apple.QD (4.0 - 413) <D2E1DC80-D26F-3508-BBA5-B8AE7ED6CB0E> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/QD.framework/Versions/A/QD
    0x7fff35f7f000 -     0x7fff35f8cffc  com.apple.speech.synthesis.framework (9.0.24 - 9.0.24) <823C0DE7-1351-3B39-8F06-AB5FCAD2C874> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/SpeechSynthesis.framework/Versions/A/SpeechSynthesis
    0x7fff35f8d000 -     0x7fff3606effa  com.apple.audio.toolbox.AudioToolbox (1.14 - 1.14) <75651F0A-F2CE-3F68-B86A-E66B8815DCF4> /System/Library/Frameworks/AudioToolbox.framework/Versions/A/AudioToolbox
    0x7fff36070000 -     0x7fff36070fff  com.apple.audio.units.AudioUnit (1.14 - 1.14) <E51DCB3C-CF4F-320B-AC86-D445E30C0891> /System/Library/Frameworks/AudioUnit.framework/Versions/A/AudioUnit
    0x7fff36406000 -     0x7fff36794ffd  com.apple.CFNetwork (1126 - 1126) <BB8F4C63-10B8-3ACD-84CF-D4DCFA9245DD> /System/Library/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
    0x7fff36815000 -     0x7fff36b09ff3  com.apple.HIToolbox (2.1.1 - 994.6) <5C44ACA7-D158-3F9B-8F88-0477510D44FA> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/HIToolbox
    0x7fff36b54000 -     0x7fff36b5afff  com.apple.speech.recognition.framework (6.0.3 - 6.0.3) <1188E643-967C-334E-BC1A-60A0F82C67E5> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SpeechRecognition.framework/Versions/A/SpeechRecognition
    0x7fff36d02000 -     0x7fff36df8fff  com.apple.ColorSync (4.13.0 - 3394.9) <61698A7B-BB8C-3891-9547-703FF84671A8> /System/Library/Frameworks/ColorSync.framework/Versions/A/ColorSync
    0x7fff370e3000 -     0x7fff375ecffb  com.apple.audio.CoreAudio (5.0 - 5.0) <62BEE4B7-8A26-3951-9D78-4E193617AF7A> /System/Library/Frameworks/CoreAudio.framework/Versions/A/CoreAudio
    0x7fff3763f000 -     0x7fff37677fff  com.apple.CoreBluetooth (1.0 - 1) <6BC7F863-4495-371F-BC54-543E5CFE1665> /System/Library/Frameworks/CoreBluetooth.framework/Versions/A/CoreBluetooth
    0x7fff37678000 -     0x7fff37a62fe8  com.apple.CoreData (120 - 977.3) <D902A2E3-1D0A-3995-974E-A526976E5735> /System/Library/Frameworks/CoreData.framework/Versions/A/CoreData
    0x7fff37a63000 -     0x7fff37b93ffe  com.apple.CoreDisplay (1.0 - 186.6.12) <EA74CC46-8715-3B90-95E8-4594D2F0CC8A> /System/Library/Frameworks/CoreDisplay.framework/Versions/A/CoreDisplay
    0x7fff37b94000 -     0x7fff38013ffb  com.apple.CoreFoundation (6.9 - 1676.105) <6AF8B3CC-BC3F-3869-B9FB-1D881422364E> /System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
    0x7fff38015000 -     0x7fff38689ff8  com.apple.CoreGraphics (2.0 - 1355.17) <E1CE3919-F36B-309F-89BA-79F36DC8125E> /System/Library/Frameworks/CoreGraphics.framework/Versions/A/CoreGraphics
    0x7fff38697000 -     0x7fff389f2ff0  com.apple.CoreImage (15.0.0 - 940.9) <44F68E8C-315A-32A6-BB19-7F24C00AB347> /System/Library/Frameworks/CoreImage.framework/Versions/A/CoreImage
    0x7fff38f7b000 -     0x7fff38f7bfff  com.apple.CoreServices (1069.24 - 1069.24) <D9F6AB40-10EC-3682-A969-85560E2E4768> /System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
    0x7fff38f7c000 -     0x7fff39001fff  com.apple.AE (838.1 - 838.1) <5F26DA9B-FB2E-3AF8-964B-63BD6671CF12> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE.framework/Versions/A/AE
    0x7fff39002000 -     0x7fff392e3ff7  com.apple.CoreServices.CarbonCore (1217 - 1217) <8022AF47-AA99-3786-B086-141D84F00387> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
    0x7fff392e4000 -     0x7fff39331ffd  com.apple.DictionaryServices (1.2 - 323.6) <C0F3830C-A4C6-3046-9A6A-DE1B5D448C2C> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/DictionaryServices.framework/Versions/A/DictionaryServices
    0x7fff39332000 -     0x7fff3933aff7  com.apple.CoreServices.FSEvents (1268.100.1 - 1268.100.1) <E4B2CAF2-1203-335F-9971-1278CB6E2AE0> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEvents.framework/Versions/A/FSEvents
    0x7fff3933b000 -     0x7fff39575ff6  com.apple.LaunchServices (1069.24 - 1069.24) <2E0AD228-B1CC-3645-91EE-EB7F46F2147B> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/LaunchServices
    0x7fff39576000 -     0x7fff3960eff1  com.apple.Metadata (10.7.0 - 2076.6) <C8034E84-7DD4-34B9-9CDF-16A05032FF39> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
    0x7fff3960f000 -     0x7fff3963cfff  com.apple.CoreServices.OSServices (1069.24 - 1069.24) <72FDEA52-7607-3745-AC43-630D80962099> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
    0x7fff3963d000 -     0x7fff396a4fff  com.apple.SearchKit (1.4.1 - 1.4.1) <086EB5DF-A2EC-3342-8028-CA7996BE5CB2> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
    0x7fff396a5000 -     0x7fff396c9ff5  com.apple.coreservices.SharedFileList (131.4 - 131.4) <AE333DA2-C279-3751-8C15-B963E58EE61E> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SharedFileList.framework/Versions/A/SharedFileList
    0x7fff399ee000 -     0x7fff39ba5ffc  com.apple.CoreText (643.1.5.1 - 643.1.5.1) <715FE3F7-E8FB-3997-85A0-3AB2839F6C30> /System/Library/Frameworks/CoreText.framework/Versions/A/CoreText
    0x7fff39ba6000 -     0x7fff39beaffb  com.apple.CoreVideo (1.8 - 344.3) <A200AFC7-2CB2-30CD-8B2A-1269CA64A29B> /System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo
    0x7fff39beb000 -     0x7fff39c78ffc  com.apple.framework.CoreWLAN (13.0 - 1601.2) <855E51AA-DF3A-3BB9-A4F0-6880D42B8762> /System/Library/Frameworks/CoreWLAN.framework/Versions/A/CoreWLAN
    0x7fff39f0f000 -     0x7fff39f15fff  com.apple.DiskArbitration (2.7 - 2.7) <52E7D181-2A18-37CD-B24F-AA32E93F7A69> /System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
    0x7fff3a108000 -     0x7fff3a236ff6  com.apple.FileProvider (304.1 - 304.1) <58D64236-947E-3E6A-85E6-C66AEF9E0EE0> /System/Library/Frameworks/FileProvider.framework/Versions/A/FileProvider
    0x7fff3a24e000 -     0x7fff3a613fff  com.apple.Foundation (6.9 - 1676.105) <1FA28BAB-7296-3A09-8E1E-E62A7D233DB8> /System/Library/Frameworks/Foundation.framework/Versions/C/Foundation
    0x7fff3a680000 -     0x7fff3a6d0ff7  com.apple.GSS (4.0 - 2.0) <4E241C00-42A5-3572-9430-D950FBB7A4A0> /System/Library/Frameworks/GSS.framework/Versions/A/GSS
    0x7fff3a80d000 -     0x7fff3a921ff3  com.apple.Bluetooth (7.0.5 - 7.0.5f6) <5897C368-9674-3E34-B144-FFB06A2DF37B> /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth
    0x7fff3a987000 -     0x7fff3aa2bff3  com.apple.framework.IOKit (2.0.2 - 1726.121.1) <A0F54725-036F-3279-A46E-C2ABDBFD479B> /System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
    0x7fff3aa2d000 -     0x7fff3aa3effb  com.apple.IOSurface (269.11 - 269.11) <D3CC2AA1-4AE2-30EE-A9DB-C04CCAA88ADE> /System/Library/Frameworks/IOSurface.framework/Versions/A/IOSurface
    0x7fff3aabd000 -     0x7fff3ac19ffe  com.apple.ImageIO.framework (3.3.0 - 1976.6) <5B4C2E04-9161-3C82-A7FB-F4D51E3C1E10> /System/Library/Frameworks/ImageIO.framework/Versions/A/ImageIO
    0x7fff3ac1a000 -     0x7fff3ac1dfff  libGIF.dylib (1976.6) <3B26EE1C-C570-305C-A9A3-EA62D2F2E7B8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libGIF.dylib
    0x7fff3ac1e000 -     0x7fff3acd7fff  libJP2.dylib (1976.6) <EB4E4E09-DD81-3E6B-A513-3667E810AEF3> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJP2.dylib
    0x7fff3acd8000 -     0x7fff3acfbfe3  libJPEG.dylib (1976.6) <9D7FAC55-85A6-34AB-9F26-0BCA381E8CE7> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJPEG.dylib
    0x7fff3af77000 -     0x7fff3af91fef  libPng.dylib (1976.6) <4886A1F8-E9CA-38F2-BF2F-1FCA1DFDD1C9> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libPng.dylib
    0x7fff3af92000 -     0x7fff3af93fff  libRadiance.dylib (1976.6) <FA759D33-131A-33B8-943E-32409F162738> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libRadiance.dylib
    0x7fff3af94000 -     0x7fff3afddfff  libTIFF.dylib (1976.6) <E9994BF8-6CF5-3422-B4FD-A14DC390EF12> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libTIFF.dylib
    0x7fff3c53f000 -     0x7fff3c551ff3  com.apple.Kerberos (3.0 - 1) <AE0E56CA-D924-3CC8-BBAA-8C6EEC3038BE> /System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
    0x7fff3c552000 -     0x7fff3c552fff  libHeimdalProxy.dylib (77) <A970C7A8-7CCD-3701-A459-078BD5E8FE4E> /System/Library/Frameworks/Kerberos.framework/Versions/A/Libraries/libHeimdalProxy.dylib
    0x7fff3d109000 -     0x7fff3d1d3ff7  com.apple.Metal (212.7 - 212.7) <CC7BF715-142B-3C2A-81AD-0E35693230F2> /System/Library/Frameworks/Metal.framework/Versions/A/Metal
    0x7fff3d1f0000 -     0x7fff3d22dff7  com.apple.MetalPerformanceShaders.MPSCore (1.0 - 1) <52089325-EC97-3EED-ABB3-9B39EC0BD429> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSCore.framework/Versions/A/MPSCore
    0x7fff3d22e000 -     0x7fff3d2b8fe2  com.apple.MetalPerformanceShaders.MPSImage (1.0 - 1) <9E434EA0-6BCA-3903-B882-CEB69730A63B> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSImage.framework/Versions/A/MPSImage
    0x7fff3d2b9000 -     0x7fff3d2deff4  com.apple.MetalPerformanceShaders.MPSMatrix (1.0 - 1) <EDD6C3A5-E231-3FB1-B4D4-45742AFB9A4E> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSMatrix.framework/Versions/A/MPSMatrix
    0x7fff3d2df000 -     0x7fff3d2f4ffb  com.apple.MetalPerformanceShaders.MPSNDArray (1.0 - 1) <C5A2B865-5CE2-3E5D-8452-54639FCB0954> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNDArray.framework/Versions/A/MPSNDArray
    0x7fff3d2f5000 -     0x7fff3d453ffc  com.apple.MetalPerformanceShaders.MPSNeuralNetwork (1.0 - 1) <47CCDBAC-5843-366A-A68C-6E8851D0865D> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNeuralNetwork.framework/Versions/A/MPSNeuralNetwork
    0x7fff3d454000 -     0x7fff3d4a3ff4  com.apple.MetalPerformanceShaders.MPSRayIntersector (1.0 - 1) <302BDF8E-B00A-3123-A6C4-E262B7513CF6> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSRayIntersector.framework/Versions/A/MPSRayIntersector
    0x7fff3d4a4000 -     0x7fff3d4a5ff5  com.apple.MetalPerformanceShaders.MetalPerformanceShaders (1.0 - 1) <14F84B42-9DA2-39A1-81B4-666B8020520C> /System/Library/Frameworks/MetalPerformanceShaders.framework/Versions/A/MetalPerformanceShaders
    0x7fff3e52c000 -     0x7fff3e538ffe  com.apple.NetFS (6.0 - 4.0) <AC74E6A4-6E9B-3AB1-9577-8277F8A3EDE0> /System/Library/Frameworks/NetFS.framework/Versions/A/NetFS
    0x7fff3e539000 -     0x7fff3e690ff3  com.apple.Network (1.0 - 1) <B5B8E999-BBCC-3DEF-8881-8FFF69F8EC4B> /System/Library/Frameworks/Network.framework/Versions/A/Network
    0x7fff410c1000 -     0x7fff41119fff  com.apple.opencl (3.5 - 3.5) <9B101D40-EA79-3C0D-B7AE-A3F18094B2D7> /System/Library/Frameworks/OpenCL.framework/Versions/A/OpenCL
    0x7fff4111a000 -     0x7fff41136fff  com.apple.CFOpenDirectory (10.15 - 220.40.1) <BFC32EBE-D95C-3267-B95C-5CEEFD189EA6> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/Frameworks/CFOpenDirectory.framework/Versions/A/CFOpenDirectory
    0x7fff41137000 -     0x7fff41142ffd  com.apple.OpenDirectory (10.15 - 220.40.1) <76A20BBA-775F-3E17-AB0F-FEDFCDCE0716> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/OpenDirectory
    0x7fff41aa8000 -     0x7fff41aaafff  libCVMSPluginSupport.dylib (17.10.22) <AAE07E0C-4B28-364B-A7D9-028C7CD14954> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCVMSPluginSupport.dylib
    0x7fff41aab000 -     0x7fff41ab0fff  libCoreFSCache.dylib (176.15) <609C5DFC-9A97-344D-BBC7-E0B08D862C63> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreFSCache.dylib
    0x7fff41ab1000 -     0x7fff41ab5fff  libCoreVMClient.dylib (176.15) <8F8DD27F-AC7C-398D-A8E3-396F1528E317> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreVMClient.dylib
    0x7fff41ab6000 -     0x7fff41abeff7  libGFXShared.dylib (17.10.22) <D0649AB5-5331-328D-8141-E5A6F06D4AD7> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGFXShared.dylib
    0x7fff41abf000 -     0x7fff41ac9fff  libGL.dylib (17.10.22) <116DDBF7-D725-3B8C-BD0B-A21B758FE421> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGL.dylib
    0x7fff41aca000 -     0x7fff41afeff7  libGLImage.dylib (17.10.22) <2B314C76-C7E6-3AC5-9157-70B0529C1F9B> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLImage.dylib
    0x7fff41c94000 -     0x7fff41cd0fff  libGLU.dylib (17.10.22) <B29F73B2-B5A9-3C38-AF77-351173D2E3DB> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLU.dylib
    0x7fff4270c000 -     0x7fff4271bff7  com.apple.opengl (17.10.22 - 17.10.22) <D5BF32A8-2E0A-345D-80A3-7CA7C2CF748D> /System/Library/Frameworks/OpenGL.framework/Versions/A/OpenGL
    0x7fff436d9000 -     0x7fff4395bff9  com.apple.QuartzCore (1.11 - 841.2) <444E6F22-DFA6-391B-B51F-A96AE69E524D> /System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore
    0x7fff444dc000 -     0x7fff44825ff1  com.apple.security (7.0 - 59306.120.7) <AEA33464-1507-36F1-8CAE-A86EB787F9B5> /System/Library/Frameworks/Security.framework/Versions/A/Security
    0x7fff44826000 -     0x7fff448aeffb  com.apple.securityfoundation (6.0 - 55236.60.1) <79289FE1-CB5F-3BEF-A33F-11A29A93A681> /System/Library/Frameworks/SecurityFoundation.framework/Versions/A/SecurityFoundation
    0x7fff448dd000 -     0x7fff448e1ff8  com.apple.xpc.ServiceManagement (1.0 - 1) <4194D29D-F0D4-33F8-839A-D03C6C62D8DB> /System/Library/Frameworks/ServiceManagement.framework/Versions/A/ServiceManagement
    0x7fff4558d000 -     0x7fff455fbff7  com.apple.SystemConfiguration (1.19 - 1.19) <0CF8726A-BE41-3E07-B895-FBC44B75450E> /System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
    0x7fff4955c000 -     0x7fff49621ff7  com.apple.APFS (1412.120.2 - 1412.120.2) <1E8FD511-FDC4-31A2-ACDE-EB5192032BC6> /System/Library/PrivateFrameworks/APFS.framework/Versions/A/APFS
    0x7fff4a731000 -     0x7fff4a732ff1  com.apple.AggregateDictionary (1.0 - 1) <B907CE7A-0122-3E63-BF39-0A242B0DD7C4> /System/Library/PrivateFrameworks/AggregateDictionary.framework/Versions/A/AggregateDictionary
    0x7fff4accc000 -     0x7fff4ace9ff4  com.apple.AppContainer (4.0 - 448.100.6) <9C2D0065-9B38-3D1C-A090-46F129A1B3CA> /System/Library/PrivateFrameworks/AppContainer.framework/Versions/A/AppContainer
    0x7fff4ad3e000 -     0x7fff4ad4cff7  com.apple.AppSandbox (4.0 - 448.100.6) <7DAB58B1-8176-3FB0-A7B0-8A38E118E90B> /System/Library/PrivateFrameworks/AppSandbox.framework/Versions/A/AppSandbox
    0x7fff4b1c7000 -     0x7fff4b1ebffb  com.apple.framework.Apple80211 (13.0 - 1610.1) <B4B80CD6-B38E-3748-B30B-5088CCE9A7A8> /System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Apple80211
    0x7fff4b4a9000 -     0x7fff4b4b8fd7  com.apple.AppleFSCompression (119.100.1 - 1.0) <2E75CF51-B693-3275-9A4F-40571D48745E> /System/Library/PrivateFrameworks/AppleFSCompression.framework/Versions/A/AppleFSCompression
    0x7fff4b5b7000 -     0x7fff4b5c2ff7  com.apple.AppleIDAuthSupport (1.0 - 1) <F4651654-E24F-3BF4-8DDF-8F91E5219BA4> /System/Library/PrivateFrameworks/AppleIDAuthSupport.framework/Versions/A/AppleIDAuthSupport
    0x7fff4b604000 -     0x7fff4b64cff7  com.apple.AppleJPEG (1.0 - 1) <4655FF70-9772-3D7C-8159-5A5E56C9F84B> /System/Library/PrivateFrameworks/AppleJPEG.framework/Versions/A/AppleJPEG
    0x7fff4ba3b000 -     0x7fff4ba5dfff  com.apple.applesauce (1.0 - 16.25) <33B66B71-64A4-365D-9953-E0545E69A5E7> /System/Library/PrivateFrameworks/AppleSauce.framework/Versions/A/AppleSauce
    0x7fff4bb1c000 -     0x7fff4bb1fffb  com.apple.AppleSystemInfo (3.1.5 - 3.1.5) <92580EE3-74BF-3488-90ED-C8EBD7A1B4C3> /System/Library/PrivateFrameworks/AppleSystemInfo.framework/Versions/A/AppleSystemInfo
    0x7fff4bbb9000 -     0x7fff4bbc8ff9  com.apple.AssertionServices (1.0 - 223.100.31) <478D2004-9B84-3AE9-9A0B-0A0B68ED028F> /System/Library/PrivateFrameworks/AssertionServices.framework/Versions/A/AssertionServices
    0x7fff4c75b000 -     0x7fff4c99bfe0  com.apple.audio.AudioToolboxCore (1.0 - 1104.84) <FA17E892-6A1A-309D-95C7-30ACFC44319C> /System/Library/PrivateFrameworks/AudioToolboxCore.framework/Versions/A/AudioToolboxCore
    0x7fff4c99f000 -     0x7fff4cabbff0  com.apple.AuthKit (1.0 - 1) <375C3886-5430-3C02-BD2C-4244BF490ABA> /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/AuthKit
    0x7fff4cc78000 -     0x7fff4cc81ff7  com.apple.coreservices.BackgroundTaskManagement (1.0 - 104) <F070F440-27AB-3FCF-9602-F278C332CA01> /System/Library/PrivateFrameworks/BackgroundTaskManagement.framework/Versions/A/BackgroundTaskManagement
    0x7fff4cc82000 -     0x7fff4cd23ff5  com.apple.backup.framework (1.11.5 - 1298.5.10) <637CA389-627A-365C-98C2-D297C47D6EE3> /System/Library/PrivateFrameworks/Backup.framework/Versions/A/Backup
    0x7fff4cd24000 -     0x7fff4cdb0ff6  com.apple.BaseBoard (466.3 - 466.3) <1718A41A-9923-3FD0-96B8-82376E153D27> /System/Library/PrivateFrameworks/BaseBoard.framework/Versions/A/BaseBoard
    0x7fff4ceb2000 -     0x7fff4ceeeff7  com.apple.bom (14.0 - 219.2) <586F1D9C-23B0-3F38-9C5B-728E9DD8B953> /System/Library/PrivateFrameworks/Bom.framework/Versions/A/Bom
    0x7fff4da6d000 -     0x7fff4dabcfff  com.apple.ChunkingLibrary (307 - 307) <32E0F1A1-9DD6-3B52-91C2-93643041AA60> /System/Library/PrivateFrameworks/ChunkingLibrary.framework/Versions/A/ChunkingLibrary
    0x7fff4e963000 -     0x7fff4e973ffb  com.apple.CommonAuth (4.0 - 2.0) <91EC83B5-857D-3D4F-93B1-AAD7E0E029D8> /System/Library/PrivateFrameworks/CommonAuth.framework/Versions/A/CommonAuth
    0x7fff4e987000 -     0x7fff4e99efff  com.apple.commonutilities (8.0 - 900) <12C6DEE5-1740-39A5-9711-6F815C6D77BD> /System/Library/PrivateFrameworks/CommonUtilities.framework/Versions/A/CommonUtilities
    0x7fff4f4c8000 -     0x7fff4f4e7ffc  com.apple.analyticsd (1.0 - 1) <0384CD57-1A3E-397A-B9F6-19761A14A7D4> /System/Library/PrivateFrameworks/CoreAnalytics.framework/Versions/A/CoreAnalytics
    0x7fff4fa7e000 -     0x7fff4fa8eff3  com.apple.CoreEmoji (1.0 - 107.1) <CDCCB4B0-B98F-38E8-9568-C81320E756EB> /System/Library/PrivateFrameworks/CoreEmoji.framework/Versions/A/CoreEmoji
    0x7fff500ce000 -     0x7fff50138ff0  com.apple.CoreNLP (1.0 - 213) <40FC46D2-844C-3282-A8E4-69DD827F05C5> /System/Library/PrivateFrameworks/CoreNLP.framework/Versions/A/CoreNLP
    0x7fff50566000 -     0x7fff5056eff8  com.apple.CorePhoneNumbers (1.0 - 1) <B2CE100D-B82F-3481-86F6-7F85FF8BFAFB> /System/Library/PrivateFrameworks/CorePhoneNumbers.framework/Versions/A/CorePhoneNumbers
    0x7fff50f5b000 -     0x7fff50f7efff  com.apple.CoreSVG (1.0 - 129) <3141D198-0507-3F72-A2C9-752EAFE3EEB3> /System/Library/PrivateFrameworks/CoreSVG.framework/Versions/A/CoreSVG
    0x7fff50f7f000 -     0x7fff50fb2fff  com.apple.CoreServicesInternal (446.7 - 446.7) <B2CEC515-2225-3CB1-B34A-904AAEA54FDF> /System/Library/PrivateFrameworks/CoreServicesInternal.framework/Versions/A/CoreServicesInternal
    0x7fff50fb3000 -     0x7fff50fe1ffd  com.apple.CSStore (1069.24 - 1069.24) <C96E5CE8-D604-3F13-B079-B2BA33B90081> /System/Library/PrivateFrameworks/CoreServicesStore.framework/Versions/A/CoreServicesStore
    0x7fff51506000 -     0x7fff5159cff7  com.apple.CoreSymbolication (11.4 - 64535.33.2) <0E7A1529-C737-33FD-AA7D-A32EB8B192DA> /System/Library/PrivateFrameworks/CoreSymbolication.framework/Versions/A/CoreSymbolication
    0x7fff51634000 -     0x7fff51760ff6  com.apple.coreui (2.1 - 609.4) <9B93CC42-804B-305A-8FCE-5F06821B544C> /System/Library/PrivateFrameworks/CoreUI.framework/Versions/A/CoreUI
    0x7fff51761000 -     0x7fff51917ff5  com.apple.CoreUtils (6.2.1 - 621.5) <CD4B58E5-1494-3457-84FF-3869378E2DC9> /System/Library/PrivateFrameworks/CoreUtils.framework/Versions/A/CoreUtils
    0x7fff51a51000 -     0x7fff51a64ff1  com.apple.CrashReporterSupport (10.13 - 15016) <827F4E31-9F23-3683-AC5A-59CCA90F2359> /System/Library/PrivateFrameworks/CrashReporterSupport.framework/Versions/A/CrashReporterSupport
    0x7fff51b1d000 -     0x7fff51b2fff8  com.apple.framework.DFRFoundation (1.0 - 252.50.1) <19F79D32-71D3-3A87-98C9-B5C6C96076C4> /System/Library/PrivateFrameworks/DFRFoundation.framework/Versions/A/DFRFoundation
    0x7fff51b30000 -     0x7fff51b35fff  com.apple.DSExternalDisplay (3.1 - 380) <971F24F1-B1FC-3674-9C00-F88EEF94DC05> /System/Library/PrivateFrameworks/DSExternalDisplay.framework/Versions/A/DSExternalDisplay
    0x7fff51bbf000 -     0x7fff51c39ff0  com.apple.datadetectorscore (8.0 - 659) <9FD9BDFA-3724-3BEA-946C-0473447196A3> /System/Library/PrivateFrameworks/DataDetectorsCore.framework/Versions/A/DataDetectorsCore
    0x7fff51c85000 -     0x7fff51cc2ff8  com.apple.DebugSymbols (194 - 194) <9BCE6685-6C45-3DF9-98EB-FCF8196160F0> /System/Library/PrivateFrameworks/DebugSymbols.framework/Versions/A/DebugSymbols
    0x7fff51cc3000 -     0x7fff51e4bff6  com.apple.desktopservices (1.14.5 - 1281.5.3) <79972B8B-7B60-3AD5-9A5F-17976DE8080B> /System/Library/PrivateFrameworks/DesktopServicesPriv.framework/Versions/A/DesktopServicesPriv
    0x7fff537f9000 -     0x7fff53c14ff1  com.apple.vision.FaceCore (4.3.0 - 4.3.0) <1B5D7DD6-718E-3111-A702-EB04B8903662> /System/Library/PrivateFrameworks/FaceCore.framework/Versions/A/FaceCore
    0x7fff542b2000 -     0x7fff543eaffc  libFontParser.dylib (277.2.5.3) <DEB18756-082F-359B-AE93-F9C3B5EC7AF4> /System/Library/PrivateFrameworks/FontServices.framework/libFontParser.dylib
    0x7fff543eb000 -     0x7fff5441ffff  libTrueTypeScaler.dylib (277.2.5.3) <B16255A6-C51A-328B-B679-8FBF64495770> /System/Library/PrivateFrameworks/FontServices.framework/libTrueTypeScaler.dylib
    0x7fff54484000 -     0x7fff54494ff6  libhvf.dylib (1.0 - $[CURRENT_PROJECT_VERSION]) <6396BC1F-13C1-37D7-91B9-1FF60910C7FA> /System/Library/PrivateFrameworks/FontServices.framework/libhvf.dylib
    0x7fff59033000 -     0x7fff59039fff  com.apple.GPUWrangler (5.2.4 - 5.2.4) <5B819701-9F0C-374B-8925-A22DFC16514F> /System/Library/PrivateFrameworks/GPUWrangler.framework/Versions/A/GPUWrangler
    0x7fff59358000 -     0x7fff5937eff1  com.apple.GenerationalStorage (2.0 - 314) <5613706F-710A-39E0-8B25-BA103A768613> /System/Library/PrivateFrameworks/GenerationalStorage.framework/Versions/A/GenerationalStorage
    0x7fff5a4ad000 -     0x7fff5a4bbffb  com.apple.GraphVisualizer (1.0 - 100.1) <0A86C9FF-4484-3C7F-BC71-3D23BDBE81CE> /System/Library/PrivateFrameworks/GraphVisualizer.framework/Versions/A/GraphVisualizer
    0x7fff5a65a000 -     0x7fff5a718ff4  com.apple.Heimdal (4.0 - 2.0) <F2C504F6-E211-3AB0-9754-D96D2F96634B> /System/Library/PrivateFrameworks/Heimdal.framework/Versions/A/Heimdal
    0x7fff5c89c000 -     0x7fff5c8a4ff5  com.apple.IOAccelerator (438.5.4 - 438.5.4) <A3CF45E0-4D88-33BF-BAEC-690D5664CDA0> /System/Library/PrivateFrameworks/IOAccelerator.framework/Versions/A/IOAccelerator
    0x7fff5c8b1000 -     0x7fff5c8c8fff  com.apple.IOPresentment (1.0 - 37) <3EDBB454-D248-394B-A026-9717CD8535C3> /System/Library/PrivateFrameworks/IOPresentment.framework/Versions/A/IOPresentment
    0x7fff5cc50000 -     0x7fff5cc9bff1  com.apple.IconServices (438.3 - 438.3) <2AE74790-64F1-3B0A-9534-DEEEE307E562> /System/Library/PrivateFrameworks/IconServices.framework/Versions/A/IconServices
    0x7fff5ce59000 -     0x7fff5ce60ff9  com.apple.InternationalSupport (1.0 - 45.4) <C22AA77A-EDA6-3626-8816-521A8CD43C4A> /System/Library/PrivateFrameworks/InternationalSupport.framework/Versions/A/InternationalSupport
    0x7fff5d0ed000 -     0x7fff5d10cffd  com.apple.security.KeychainCircle.KeychainCircle (1.0 - 1) <76DB5326-BE5D-3339-975C-D9FCF39A341E> /System/Library/PrivateFrameworks/KeychainCircle.framework/Versions/A/KeychainCircle
    0x7fff5d241000 -     0x7fff5d30fffd  com.apple.LanguageModeling (1.0 - 215.1) <A6FAA215-9A01-3EE1-B304-2238801C5883> /System/Library/PrivateFrameworks/LanguageModeling.framework/Versions/A/LanguageModeling
    0x7fff5d310000 -     0x7fff5d358fff  com.apple.Lexicon-framework (1.0 - 72) <6AE1872C-0352-36FE-90CC-7303F13A5BEF> /System/Library/PrivateFrameworks/Lexicon.framework/Versions/A/Lexicon
    0x7fff5d35f000 -     0x7fff5d364ff3  com.apple.LinguisticData (1.0 - 353.18) <686E7B7C-640F-3D7B-A9C1-31E2DFACD457> /System/Library/PrivateFrameworks/LinguisticData.framework/Versions/A/LinguisticData
    0x7fff5e6cb000 -     0x7fff5e717fff  com.apple.spotlight.metadata.utilities (1.0 - 2076.6) <C3AEA22D-1FEB-3E38-9821-1FA447C8AF9D> /System/Library/PrivateFrameworks/MetadataUtilities.framework/Versions/A/MetadataUtilities
    0x7fff5e718000 -     0x7fff5e7e9ffa  com.apple.gpusw.MetalTools (1.0 - 1) <A23FFAB6-437C-303C-A3E8-99F323F8E734> /System/Library/PrivateFrameworks/MetalTools.framework/Versions/A/MetalTools
    0x7fff5ea1d000 -     0x7fff5ea3bfff  com.apple.MobileKeyBag (2.0 - 1.0) <B564179F-A5E9-3BD8-892D-CEF2BE0DA0D7> /System/Library/PrivateFrameworks/MobileKeyBag.framework/Versions/A/MobileKeyBag
    0x7fff5ec9e000 -     0x7fff5ecceff7  com.apple.MultitouchSupport.framework (3440.1 - 3440.1) <4E7CB188-382E-3128-8671-4A3EF6E06622> /System/Library/PrivateFrameworks/MultitouchSupport.framework/Versions/A/MultitouchSupport
    0x7fff5f1ce000 -     0x7fff5f1d8fff  com.apple.NetAuth (6.2 - 6.2) <D660F2CB-5A49-3DD0-9DB3-86EF0797828C> /System/Library/PrivateFrameworks/NetAuth.framework/Versions/A/NetAuth
    0x7fff5fbee000 -     0x7fff5fc39ffb  com.apple.OTSVG (1.0 - 643.1.5.1) <CD494F00-9AA4-3A48-916B-EA8661F9772D> /System/Library/PrivateFrameworks/OTSVG.framework/Versions/A/OTSVG
    0x7fff60e4d000 -     0x7fff60e58ff2  com.apple.PerformanceAnalysis (1.243.2 - 243.2) <941698D6-EF00-3D59-8560-F160BC04B412> /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/PerformanceAnalysis
    0x7fff60e59000 -     0x7fff60e81ffb  com.apple.persistentconnection (1.0 - 1.0) <FFC20BB6-9CF3-39F2-AE5C-8DEE348FE0C1> /System/Library/PrivateFrameworks/PersistentConnection.framework/Versions/A/PersistentConnection
    0x7fff6383f000 -     0x7fff63858ffb  com.apple.ProtocolBuffer (1 - 274.24.9.16.3) <A2E34123-7CED-378B-A43D-1B98B5B85F5C> /System/Library/PrivateFrameworks/ProtocolBuffer.framework/Versions/A/ProtocolBuffer
    0x7fff63cb7000 -     0x7fff63ce0ff1  com.apple.RemoteViewServices (2.0 - 148) <1C61CFC2-F76F-31E5-BA13-EFD5DC69C8D5> /System/Library/PrivateFrameworks/RemoteViewServices.framework/Versions/A/RemoteViewServices
    0x7fff63e45000 -     0x7fff63e80ff0  com.apple.RunningBoardServices (1.0 - 223.100.31) <9FD1FC53-186A-3327-A359-B0BC7F4360EF> /System/Library/PrivateFrameworks/RunningBoardServices.framework/Versions/A/RunningBoardServices
    0x7fff65760000 -     0x7fff65763ff5  com.apple.SecCodeWrapper (4.0 - 448.100.6) <813B3E57-6A95-3D7B-9094-C65D687A72B8> /System/Library/PrivateFrameworks/SecCodeWrapper.framework/Versions/A/SecCodeWrapper
    0x7fff658d6000 -     0x7fff659fdff0  com.apple.Sharing (1526.31 - 1526.31) <2CB07F08-7794-3BF2-9ED5-BAB5C55C9D2C> /System/Library/PrivateFrameworks/Sharing.framework/Versions/A/Sharing
    0x7fff66e10000 -     0x7fff67106ff7  com.apple.SkyLight (1.600.0 - 451.4) <F5B9065A-E975-36D1-8D53-9FBF02171FAB> /System/Library/PrivateFrameworks/SkyLight.framework/Versions/A/SkyLight
    0x7fff67953000 -     0x7fff67961ffb  com.apple.SpeechRecognitionCore (6.0.91.2 - 6.0.91.2) <820602AB-117B-3C3E-B20B-819CBC97B7A4> /System/Library/PrivateFrameworks/SpeechRecognitionCore.framework/Versions/A/SpeechRecognitionCore
    0x7fff68193000 -     0x7fff6819cff7  com.apple.SymptomDiagnosticReporter (1.0 - 1238.120.1) <581F31D6-94CB-38AA-BD56-1A63606E516E> /System/Library/PrivateFrameworks/SymptomDiagnosticReporter.framework/Versions/A/SymptomDiagnosticReporter
    0x7fff68453000 -     0x7fff68463ff3  com.apple.TCC (1.0 - 1) <FD146B21-6DC0-3B66-BB95-57A5016B1365> /System/Library/PrivateFrameworks/TCC.framework/Versions/A/TCC
    0x7fff68988000 -     0x7fff68a4eff0  com.apple.TextureIO (3.10.9 - 3.10.9) <E23E05ED-8190-3564-985E-446F15CB0709> /System/Library/PrivateFrameworks/TextureIO.framework/Versions/A/TextureIO
    0x7fff68c13000 -     0x7fff68e6bff0  com.apple.UIFoundation (1.0 - 662) <DFD3DD4A-E661-3A12-BB02-06898949617D> /System/Library/PrivateFrameworks/UIFoundation.framework/Versions/A/UIFoundation
    0x7fff69ade000 -     0x7fff69afeffc  com.apple.UserManagement (1.0 - 1) <E168206A-9DC1-3C67-A9A6-52D816DBD5B1> /System/Library/PrivateFrameworks/UserManagement.framework/Versions/A/UserManagement
    0x7fff6a8aa000 -     0x7fff6a994ff8  com.apple.ViewBridge (464.1 - 464.1) <C698BAC9-26C8-39F1-BBC0-F57EBD2EFD7B> /System/Library/PrivateFrameworks/ViewBridge.framework/Versions/A/ViewBridge
    0x7fff6ab3a000 -     0x7fff6ab3bfff  com.apple.WatchdogClient.framework (1.0 - 67.120.2) <3B8EBB6B-77D0-317C-A3DB-D0D2E294B18D> /System/Library/PrivateFrameworks/WatchdogClient.framework/Versions/A/WatchdogClient
    0x7fff6b768000 -     0x7fff6b76bffa  com.apple.dt.XCTTargetBootstrap (1.0 - 16091) <D4F09EC7-C63F-3861-B5DA-3F7C0DFF153D> /System/Library/PrivateFrameworks/XCTTargetBootstrap.framework/Versions/A/XCTTargetBootstrap
    0x7fff6b7e5000 -     0x7fff6b7f3ff5  com.apple.audio.caulk (1.0 - 32.3) <7D3D2F91-8B1D-3558-B324-45BDF11306DB> /System/Library/PrivateFrameworks/caulk.framework/Versions/A/caulk
    0x7fff6bb35000 -     0x7fff6bb37ff3  com.apple.loginsupport (1.0 - 1) <31F02734-1ECF-37D9-9DF6-7C3BC3A324FE> /System/Library/PrivateFrameworks/login.framework/Versions/A/Frameworks/loginsupport.framework/Versions/A/loginsupport
    0x7fff6bb38000 -     0x7fff6bb4bffd  com.apple.login (3.0 - 3.0) <1DC570FD-29EC-3AE8-BD34-D44C00E4621B> /System/Library/PrivateFrameworks/login.framework/Versions/A/login
    0x7fff6e61b000 -     0x7fff6e64effa  libAudioToolboxUtility.dylib (1104.84) <BB234563-F952-3E7F-B630-4140DB5E0380> /usr/lib/libAudioToolboxUtility.dylib
    0x7fff6e655000 -     0x7fff6e689fff  libCRFSuite.dylib (48) <02C52318-C537-3FD8-BBC4-E5BD25430652> /usr/lib/libCRFSuite.dylib
    0x7fff6e68c000 -     0x7fff6e696fff  libChineseTokenizer.dylib (34) <04A7CB5A-FD68-398A-A206-33A510C115E7> /usr/lib/libChineseTokenizer.dylib
    0x7fff6e722000 -     0x7fff6e724ff7  libDiagnosticMessagesClient.dylib (112) <27220E98-6CE2-33E3-BD48-3CC3CE4AA036> /usr/lib/libDiagnosticMessagesClient.dylib
    0x7fff6e76a000 -     0x7fff6e921ffb  libFosl_dynamic.dylib (100.4) <68038226-8CAA-36B5-B5D6-510F900B318D> /usr/lib/libFosl_dynamic.dylib
    0x7fff6e948000 -     0x7fff6e94eff3  libIOReport.dylib (54) <E3A2148E-C5B2-30A8-ACD3-E081DD02BE90> /usr/lib/libIOReport.dylib
    0x7fff6ea30000 -     0x7fff6ea37fff  libMatch.1.dylib (36) <081FB6A9-0482-3697-A98B-6821FF476C6E> /usr/lib/libMatch.1.dylib
    0x7fff6ea66000 -     0x7fff6ea86fff  libMobileGestalt.dylib (826.120.5) <1977AD00-533A-31AA-8D74-EA6CB962F668> /usr/lib/libMobileGestalt.dylib
    0x7fff6ebf8000 -     0x7fff6ebf9fff  libSystem.B.dylib (1281.100.1) <DC04B185-E3C9-33AF-B450-EF3ED07FB021> /usr/lib/libSystem.B.dylib
    0x7fff6ec86000 -     0x7fff6ec87fff  libThaiTokenizer.dylib (3) <97DC10ED-3C11-3C89-B366-299A644035E7> /usr/lib/libThaiTokenizer.dylib
    0x7fff6ec9f000 -     0x7fff6ecb5fff  libapple_nghttp2.dylib (1.39.2) <B99D7150-D4E2-31A2-A594-36DA4B90D558> /usr/lib/libapple_nghttp2.dylib
    0x7fff6ecea000 -     0x7fff6ed5cff7  libarchive.2.dylib (72.100.1) <20B70252-0C4B-3AFD-8C8D-F51921E9D324> /usr/lib/libarchive.2.dylib
    0x7fff6ed5d000 -     0x7fff6edf6fe5  libate.dylib (3.0.1) <C4A5AE88-4E44-36FA-AD3E-0629AF9B94E0> /usr/lib/libate.dylib
    0x7fff6edfa000 -     0x7fff6edfaff3  libauto.dylib (187) <85383E24-1592-36BC-BB39-308B7F1C826E> /usr/lib/libauto.dylib
    0x7fff6eec0000 -     0x7fff6eed0ffb  libbsm.0.dylib (60.100.1) <B2331E11-3CBB-3BCF-93A6-12627AE444D0> /usr/lib/libbsm.0.dylib
    0x7fff6eed1000 -     0x7fff6eeddfff  libbz2.1.0.dylib (44) <BF40E193-8856-39B7-98F8-7A17B328B1E9> /usr/lib/libbz2.1.0.dylib
    0x7fff6eede000 -     0x7fff6ef30fff  libc++.1.dylib (902.1) <AD0805FE-F98B-3E2F-B072-83782B22DAC9> /usr/lib/libc++.1.dylib
    0x7fff6ef31000 -     0x7fff6ef46ffb  libc++abi.dylib (902) <771E9263-E832-3985-9477-8F1B2D73B771> /usr/lib/libc++abi.dylib
    0x7fff6ef47000 -     0x7fff6ef47fff  libcharset.1.dylib (59) <FF23D4ED-A5AD-3592-9574-48486C7DF85B> /usr/lib/libcharset.1.dylib
    0x7fff6ef48000 -     0x7fff6ef59fff  libcmph.dylib (8) <296A51E6-9661-3AC2-A1C9-F1E3510F91AA> /usr/lib/libcmph.dylib
    0x7fff6ef5a000 -     0x7fff6ef71fd7  libcompression.dylib (87) <21F37C2E-B9AA-38CE-9023-B763C8828AC6> /usr/lib/libcompression.dylib
    0x7fff6f24b000 -     0x7fff6f261ff7  libcoretls.dylib (167) <9E5D1E0C-03F8-37B6-82A1-0D0597021CB8> /usr/lib/libcoretls.dylib
    0x7fff6f262000 -     0x7fff6f263fff  libcoretls_cfhelpers.dylib (167) <C23BE09B-85D1-3744-9E7B-E2B11ACD5442> /usr/lib/libcoretls_cfhelpers.dylib
    0x7fff6f820000 -     0x7fff6f87fff7  libcups.2.dylib (483.6) <FFE8FE67-A6CC-3C38-A9D7-D411D7B562F1> /usr/lib/libcups.2.dylib
    0x7fff6f989000 -     0x7fff6f989fff  libenergytrace.dylib (21) <DBF8BDEE-7229-3F06-AC10-A28DCC4243C0> /usr/lib/libenergytrace.dylib
    0x7fff6f98a000 -     0x7fff6f9a2fff  libexpat.1.dylib (19.60.2) <C9163264-BA81-3CC6-9B8C-48A9A0709DD5> /usr/lib/libexpat.1.dylib
    0x7fff6f9b0000 -     0x7fff6f9b2fff  libfakelink.dylib (149.1) <122F530F-F10E-3DD5-BBEA-91796BE583F3> /usr/lib/libfakelink.dylib
    0x7fff6f9c1000 -     0x7fff6f9c6fff  libgermantok.dylib (24) <DD279BF6-E906-30D3-A69E-DC797E95F147> /usr/lib/libgermantok.dylib
    0x7fff6f9c7000 -     0x7fff6f9d0ff7  libheimdal-asn1.dylib (564.100.1) <68FA1BE5-8FFC-3345-8980-8D8629EBA451> /usr/lib/libheimdal-asn1.dylib
    0x7fff6f9d1000 -     0x7fff6fac1fff  libiconv.2.dylib (59) <F58FED71-6CCA-30E8-9A51-13E9B46E568D> /usr/lib/libiconv.2.dylib
    0x7fff6fac2000 -     0x7fff6fd19fff  libicucore.A.dylib (64260.0.1) <7B9204AC-EA14-3FF3-B6B9-4C85B37EED79> /usr/lib/libicucore.A.dylib
    0x7fff6fd33000 -     0x7fff6fd34fff  liblangid.dylib (133) <36581D30-1C7B-3A58-AA07-36237BD75E0E> /usr/lib/liblangid.dylib
    0x7fff6fd35000 -     0x7fff6fd4dff3  liblzma.5.dylib (16) <4DB30730-DBD1-3503-957A-D604049B98F9> /usr/lib/liblzma.5.dylib
    0x7fff6fd65000 -     0x7fff6fe0cff7  libmecab.dylib (883.11) <66AD729B-2BCC-3347-B9B3-FD88570E884D> /usr/lib/libmecab.dylib
    0x7fff6fe0d000 -     0x7fff7006fff1  libmecabra.dylib (883.11) <2AE744D2-AC95-3720-8E66-4F9C7A79384C> /usr/lib/libmecabra.dylib
    0x7fff703dc000 -     0x7fff7040bfff  libncurses.5.4.dylib (57) <B0B09D08-A7C9-38E9-8BF4-E4F4882F93A6> /usr/lib/libncurses.5.4.dylib
    0x7fff7053b000 -     0x7fff709b7ff5  libnetwork.dylib (1880.120.4) <715FB943-BA01-351C-BEA6-121970472985> /usr/lib/libnetwork.dylib
    0x7fff70a58000 -     0x7fff70a8bfde  libobjc.A.dylib (787.1) <CA836D3E-4595-33F1-B70C-7E39A3FBBE16> /usr/lib/libobjc.A.dylib
    0x7fff70a9e000 -     0x7fff70aa2fff  libpam.2.dylib (25.100.1) <732E8D8E-C630-3EC2-B6C3-A1564E3B68B8> /usr/lib/libpam.2.dylib
    0x7fff70aa5000 -     0x7fff70adbff7  libpcap.A.dylib (89.120.1) <CF2ADF15-2D44-3A35-94B4-DD24052F9B23> /usr/lib/libpcap.A.dylib
    0x7fff70b5f000 -     0x7fff70b77fff  libresolv.9.dylib (67.40.1) <B0F5D204-7EF2-3B0B-90EF-BB4D196FCC62> /usr/lib/libresolv.9.dylib
    0x7fff70b79000 -     0x7fff70bbdff7  libsandbox.1.dylib (1217.120.7) <728BC15F-9A6C-3634-9427-60C01CB5A5D6> /usr/lib/libsandbox.1.dylib
    0x7fff70bd3000 -     0x7fff70dbdff7  libsqlite3.dylib (308.5) <AF518115-4AD1-39F2-9B82-E2640E2221E1> /usr/lib/libsqlite3.dylib
    0x7fff7100e000 -     0x7fff71011ffb  libutil.dylib (57) <D33B63D2-ADC2-38BD-B8F2-24056C41E07B> /usr/lib/libutil.dylib
    0x7fff71012000 -     0x7fff7101fff7  libxar.1.dylib (425.2) <943A4CBB-331B-3A04-A11F-A2301189D40B> /usr/lib/libxar.1.dylib
    0x7fff71025000 -     0x7fff71107ff7  libxml2.2.dylib (33.3) <262EF7C6-7D83-3C01-863F-36E97F5ACD34> /usr/lib/libxml2.2.dylib
    0x7fff7110b000 -     0x7fff71133fff  libxslt.1.dylib (16.9) <86FE4382-BD77-3C19-A678-11EBCD70685A> /usr/lib/libxslt.1.dylib
    0x7fff71134000 -     0x7fff71146ff3  libz.1.dylib (76) <DB120508-3BED-37A8-B439-5235EAB4618A> /usr/lib/libz.1.dylib
    0x7fff719f4000 -     0x7fff719f9ff3  libcache.dylib (83) <A5ECC751-A681-30D8-B33C-D192C15D25C8> /usr/lib/system/libcache.dylib
    0x7fff719fa000 -     0x7fff71a05fff  libcommonCrypto.dylib (60165.120.1) <C321A74A-AA91-3785-BEBF-BEDC6975026C> /usr/lib/system/libcommonCrypto.dylib
    0x7fff71a06000 -     0x7fff71a0dfff  libcompiler_rt.dylib (101.2) <652A6012-7E5C-3F4F-9438-86BC094526F3> /usr/lib/system/libcompiler_rt.dylib
    0x7fff71a0e000 -     0x7fff71a17ff7  libcopyfile.dylib (166.40.1) <40113A69-A81C-3397-ADC6-1D16B9A22C3E> /usr/lib/system/libcopyfile.dylib
    0x7fff71a18000 -     0x7fff71aaafe3  libcorecrypto.dylib (866.120.3) <5E4B0E50-24DD-3E04-9374-EDA9FFD6257B> /usr/lib/system/libcorecrypto.dylib
    0x7fff71bb7000 -     0x7fff71bf7ff0  libdispatch.dylib (1173.100.2) <201EDBF3-0B36-31BA-A7CB-443CE35C05D4> /usr/lib/system/libdispatch.dylib
    0x7fff71bf8000 -     0x7fff71c2efff  libdyld.dylib (750.5) <7E711A46-5E4D-393C-AEA6-440E2A5CCD0C> /usr/lib/system/libdyld.dylib
    0x7fff71c2f000 -     0x7fff71c2fffb  libkeymgr.dylib (30) <52662CAA-DB1F-30A3-BE13-D6274B1A6D7B> /usr/lib/system/libkeymgr.dylib
    0x7fff71c30000 -     0x7fff71c3cff3  libkxld.dylib (6153.121.1) <F4434EE5-E521-3481-83FC-62D57DEB6B3D> /usr/lib/system/libkxld.dylib
    0x7fff71c3d000 -     0x7fff71c3dff7  liblaunch.dylib (1738.120.8) <07CF647B-F9DC-3907-AD98-2F85FCB34A72> /usr/lib/system/liblaunch.dylib
    0x7fff71c3e000 -     0x7fff71c43ff7  libmacho.dylib (959.0.1) <D91DFF00-E22F-3796-8A1C-4C1F5F8FA03C> /usr/lib/system/libmacho.dylib
    0x7fff71c44000 -     0x7fff71c46ff3  libquarantine.dylib (110.40.3) <D3B7D02C-7646-3FB4-8529-B36DCC2419EA> /usr/lib/system/libquarantine.dylib
    0x7fff71c47000 -     0x7fff71c48ff7  libremovefile.dylib (48) <B5E88D9B-C2BE-3496-BBB2-C996317E18A3> /usr/lib/system/libremovefile.dylib
    0x7fff71c49000 -     0x7fff71c60ff3  libsystem_asl.dylib (377.60.2) <1170348D-2491-33F1-AA79-E2A05B4A287C> /usr/lib/system/libsystem_asl.dylib
    0x7fff71c61000 -     0x7fff71c61ff7  libsystem_blocks.dylib (74) <7AFBCAA6-81BE-36C3-8DB0-AAE0A4ACE4C5> /usr/lib/system/libsystem_blocks.dylib
    0x7fff71c62000 -     0x7fff71ce9fff  libsystem_c.dylib (1353.100.2) <935DDCE9-4ED0-3F79-A05A-A123DDE399CC> /usr/lib/system/libsystem_c.dylib
    0x7fff71cea000 -     0x7fff71cedffb  libsystem_configuration.dylib (1061.120.2) <EA9BC2B1-5001-3463-9FAF-39FF61CAC87C> /usr/lib/system/libsystem_configuration.dylib
    0x7fff71cee000 -     0x7fff71cf1fff  libsystem_coreservices.dylib (114) <3D0A3AA8-8415-37B2-AAE3-66C03BCE8B55> /usr/lib/system/libsystem_coreservices.dylib
    0x7fff71cf2000 -     0x7fff71cfafff  libsystem_darwin.dylib (1353.100.2) <6EEC9975-EE3B-3C95-AA5B-030FD10587BC> /usr/lib/system/libsystem_darwin.dylib
    0x7fff71cfb000 -     0x7fff71d02fff  libsystem_dnssd.dylib (1096.100.3) <0115092A-E61B-317D-8670-41C7C34B1A82> /usr/lib/system/libsystem_dnssd.dylib
    0x7fff71d03000 -     0x7fff71d04ffb  libsystem_featureflags.dylib (17) <AFDB5095-0472-34AC-BA7E-497921BF030A> /usr/lib/system/libsystem_featureflags.dylib
    0x7fff71d05000 -     0x7fff71d52ff7  libsystem_info.dylib (538) <851693E9-C079-3547-AD41-353F8C248BE8> /usr/lib/system/libsystem_info.dylib
    0x7fff71d53000 -     0x7fff71d7fff7  libsystem_kernel.dylib (6153.121.1) <84D09AE3-2DA8-3F6D-ACEC-DC4990B1A2FF> /usr/lib/system/libsystem_kernel.dylib
    0x7fff71d80000 -     0x7fff71dc7fff  libsystem_m.dylib (3178) <436CFF76-6A99-36F2-A3B6-8D017396A050> /usr/lib/system/libsystem_m.dylib
    0x7fff71dc8000 -     0x7fff71deffff  libsystem_malloc.dylib (283.100.6) <D4BA7DF2-57AC-33B0-B948-A688EE43C799> /usr/lib/system/libsystem_malloc.dylib
    0x7fff71df0000 -     0x7fff71dfdffb  libsystem_networkextension.dylib (1095.120.6) <6DE86DB0-8CD2-361E-BD6A-A34282B47847> /usr/lib/system/libsystem_networkextension.dylib
    0x7fff71dfe000 -     0x7fff71e07ff7  libsystem_notify.dylib (241.100.2) <7E9E2FC8-DF26-340C-B196-B81B11850C46> /usr/lib/system/libsystem_notify.dylib
    0x7fff71e08000 -     0x7fff71e10fef  libsystem_platform.dylib (220.100.1) <736920EA-6AE0-3B1B-BBDA-7DCDF0C229DF> /usr/lib/system/libsystem_platform.dylib
    0x7fff71e11000 -     0x7fff71e1bfff  libsystem_pthread.dylib (416.100.3) <77488669-19A3-3993-AD65-CA5377E2475A> /usr/lib/system/libsystem_pthread.dylib
    0x7fff71e1c000 -     0x7fff71e20ff3  libsystem_sandbox.dylib (1217.120.7) <20C93D69-6452-3C82-9521-8AE54345C66F> /usr/lib/system/libsystem_sandbox.dylib
    0x7fff71e21000 -     0x7fff71e23fff  libsystem_secinit.dylib (62.100.2) <E851113D-D5B1-3FB0-9D29-9C7647A71961> /usr/lib/system/libsystem_secinit.dylib
    0x7fff71e24000 -     0x7fff71e2bffb  libsystem_symptoms.dylib (1238.120.1) <25C3866B-004E-3621-9CD3-B1E9C4D887EB> /usr/lib/system/libsystem_symptoms.dylib
    0x7fff71e2c000 -     0x7fff71e42ff2  libsystem_trace.dylib (1147.120) <A1ED1D3A-5FAD-3559-A1D6-1BE4E1C5756A> /usr/lib/system/libsystem_trace.dylib
    0x7fff71e44000 -     0x7fff71e49ff7  libunwind.dylib (35.4) <253A12E2-F88F-3838-A666-C5306F833CB8> /usr/lib/system/libunwind.dylib
    0x7fff71e4a000 -     0x7fff71e7fffe  libxpc.dylib (1738.120.8) <68D433B6-DCFF-385D-8620-F847FB7D4A5A> /usr/lib/system/libxpc.dylib

External Modification Summary:
  Calls made by other processes targeting this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by all processes on this machine:
    task_for_pid: 570159
    thread_create: 0
    thread_set_state: 0

VM Region Summary:
ReadOnly portion of Libraries: Total=550.0M resident=0K(0%) swapped_out_or_unallocated=550.0M(100%)
Writable regions: Total=121.9M written=0K(0%) resident=0K(0%) swapped_out=0K(0%) unallocated=121.9M(100%)

                                VIRTUAL   REGION
REGION TYPE                        SIZE    COUNT (non-coalesced)
===========                     =======  =======
Accelerate framework               128K        1
Activity Tracing                   256K        1
CG image                            24K        2
CoreAnimation                       64K        6
CoreGraphics                         8K        1
CoreServices                        44K        1
CoreUI image data                  392K        4
Dispatch continuations            16.0M        1
Foundation                           4K        1
Kernel Alloc Once                    8K        1
MALLOC                            93.6M       51
MALLOC guard page                   32K        8
Memory Tag 242                      12K        1
STACK GUARD                       56.0M        8
Stack                             11.6M        8
VM_ALLOCATE                         72K        5
__DATA                            22.6M      265
__DATA_CONST                        20K        1
__FONT_DATA                          4K        1
__LINKEDIT                       388.9M        5
__OBJC_RO                         32.2M        1
__OBJC_RW                         1892K        2
__TEXT                           161.1M      257
__UNICODE                          564K        1
mapped file                       55.7M       14
shared memory                      644K       15
===========                     =======  =======
TOTAL                            841.7M      662

Model: MacBookPro13,3, BootROM 267.0.0.0.0, 4 processors, Quad-Core Intel Core i7, 2.9 GHz, 16 GB, SMC 2.38f11
Graphics: kHW_IntelHDGraphics530Item, Intel HD Graphics 530, spdisplays_builtin
Graphics: kHW_AMDRadeonPro460Item, AMD Radeon Pro 460, spdisplays_pcie_device, 4 GB
Memory Module: BANK 0/DIMM0, 8 GB, LPDDR3, 2133 MHz, 0x802C, 0x4D5435324C31473332443450472D30393320
Memory Module: BANK 1/DIMM0, 8 GB, LPDDR3, 2133 MHz, 0x802C, 0x4D5435324C31473332443450472D30393320
AirPort: spairport_wireless_card_type_airport_extreme (0x14E4, 0x15A), Broadcom BCM43xx 1.0 (7.77.111.1 AirPortDriverBrcmNIC-1610.1)
Bluetooth: Version 7.0.5f6, 3 services, 27 devices, 1 incoming serial ports
Network Service: Thunderbolt Ethernet Slot  1, Ethernet, en5
PCI Card: pci1b73,1100, sppci_usbxhci, Thunderbolt@70,0,0
PCI Card: pci1b73,1100, sppci_usbxhci, Thunderbolt@71,0,0
PCI Card: pci1b21,1242, sppci_usbxhci, Thunderbolt@69,0,0
PCI Card: ethernet, sppci_ethernet, Thunderbolt@68,0,0
USB Device: USB 3.0 Bus
USB Device: Apple T1 Controller
USB Device: USB 3.1 Bus
USB Device: USB 3.0 Bus
USB Device: Card Reader
USB Device: USB 3.0 Bus
USB Device: CalDigit Thunderbolt 3 Audio
Thunderbolt Bus: MacBook Pro, Apple Inc., 41.2
Thunderbolt Device: TS3 Plus, CalDigit, Inc., 1, 35.1
Thunderbolt Bus: MacBook Pro, Apple Inc., 41.2
```
