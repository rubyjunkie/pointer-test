class FileSystem
  # takes either a file path or NSUrl and creates a security app-scoped bookmark
  #------------------------------------------------------------------------------
  def self.create_security_bookmark(url)
    url           = NSURL.fileURLWithPath(url) if url.is_a? String
    error         = Pointer.new(:object)
    bookmark_data = url.bookmarkDataWithOptions(
      NSURLBookmarkCreationWithSecurityScope,
      includingResourceValuesForKeys: nil,
      relativeToURL: nil,
      error: error
    )

    # This is the original way, that works with RM 7.4
    # It does NOT work in 7.5 or 7.6
    unless bookmark_data
      puts "\nFileSystem.create_security_bookmark"
      puts "  #{error[0].localizedDescription}"
      puts "  (code: #{error[0].code}, domain: #{error[0].domain}, userInfo: #{error[0].userInfo})"
      puts "  #{url.path}"
    end

    return bookmark_data
  end

  def self.mkdir_p(path)
    error   = Pointer.new(:object)
    success = NSFileManager.defaultManager.createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil, error: error)

    unless success
      puts "\nFileSystem.mkdir_p"
      puts "  #{error[0].localizedDescription}"
      puts "  (code: #{error[0].code}, domain: #{error[0].domain}, userInfo: #{error[0].userInfo})"
    end

    success
  end
end
