describe "FileSystem" do

  it 'does not resolve a security app-scoped bookmark with a missing target' do
    bookmark  = FileSystem.create_security_bookmark('test_dir')

    bookmark.should == nil
  end
end